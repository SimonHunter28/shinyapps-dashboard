source("libraries.R",  local = TRUE)
source("core_functions.R",  local = TRUE)
source("user-module.R")
source("userid-input.R")
source("cohortID-module.R")
source("cohort-module.R")
source("population-module.R")

debug = T

Logged = if(debug==T){T}else{F};
PASSWORD <- data.frame(userName = "admin", 
                       password = "2373aadaaaea91204e5a097df8d2dd56")

# read data from the ./data folder
teams <- if(debug==TRUE){stream_in(file("Data_source/team.json"))}else{get.Data("team")}
health_report <- if(debug==TRUE){stream_in(file("Data_source/health_report.json"))}else{get.Data("health_report")}
weightData <- if(debug==TRUE){stream_in(file("Data_source/body_measurement.json"))}else{get.Data("body_measurement")}
user_data <- if(debug==TRUE){stream_in(file("Data_source/user.json"))}else{get.Data("user")}
user_data <- user_data[complete.cases(user_data$team_id),]  # only use data for people that have a team_id
population.data <- pull.population.data(user_data)  # pull the data for all the users

active_teams <- as.numeric(as.character(teams$id[teams$legitimate == TRUE & as.Date(teams$created_at) < Sys.Date() ]))
non_starters <- c(41,45,46,47,53)
fake_users <- c(21,48,50,93,94,265,261,270,271,500,497,596,597,632,692)
withdrawn_users <- c(682)

user_data <- user_data %>%
  filter(!(id %in% c(non_starters, fake_users, withdrawn_users)), legit == TRUE)  # filter both hardcoded and those marked as not legit.


filter.user.points <- function(population_data, user)  {
  data <- population_data
  User_id <- user
  team = user_data$team_id[user_data$id == User_id]
  start_date <- cohortInfo(team=team)$startDate
  message(paste("start date =",start_date))
  
  data <- data %>%
    filter(user_id == User_id) %>%
    mutate(occurred_at = as.Date(occurred_at_local),
      week = ceiling(as.numeric(occurred_at + 1 - start_date) / 7), 
#            week = as.numeric(format(created_at-1, "%U")) - start_wk + 1,
      points = as.numeric(as.character(points_awarded))) %>%
    select(points, occurred_at, week) %>%
    filter(week > 0) %>%
    arrange(occurred_at)
  
  return(list(data, start_date, User_id))
}

user.points <- function(population_data, user)  {
  message("Getting user.points")
  all.data <- filter.user.points(population_data, user)
  data <- all.data[[1]]
  start_date <- all.data[[2]]
  User_id <- all.data[[3]]
  
  days <- format(data$occurred_at[which(data$week == ceiling(as.numeric(Sys.Date() + 1 - start_date) / 7))], "%a") 
  sum_points <- cumsum(data$points[data$week == ceiling(as.numeric(Sys.Date() + 1 - start_date) / 7)])  

  week.points <- data.frame("Day" = days, "Points" = sum_points)
  message("Got user.points")
  
  return(week.points)
}

user.weekScore <- function(population_data, user) {
  message("Getting user.weekscore")
  all.data <- filter.user.points(population_data, user)
  data <- all.data[[1]]
  start_date <- all.data[[2]]
  User_id <- all.data[[3]]
  
  weeks <- unique(data$week)
  score <- sapply(weeks, function(x) sum(data$points[data$week == x]))
  curr_wk <- rep(ceiling(as.numeric(Sys.Date() + 1 - start_date) / 7), length(score)) + 1
  
  week_score <- data.frame("Week" = weeks, "Score" = score, "curr_wk" = curr_wk)
  message("Got user.weekScore")
  
  return(week_score)
    
}

active.times <- function(population_data, by, id) {
  message("Getting active times")
  if(by == 'user'){
#     User_id <- as.numeric(as.character(user_data$id[paste(user_data$first_name,user_data$last_name) == id]))
    User_id <- id
    data <- filter(population_data, user_id == User_id)
  } else if(by == 'cohort'){  
#     team <- cohortInfo(cohort=id)$team
    team <- id
    data <- population_data[population_data$user_id %in% user_data$id[user_data$team_id == team], ]
  } else {
    message("error in active.times, 'by' not selected")
  }

  # data <- mutate(data, created_at = parse_iso_8601(created_at.x), #parse_iso_8601 is crazy slow 
  data <- mutate(data, hour = as.numeric(strftime(occurred_at_local, format="%H"))) %>%
    select(hour, occurred_at)
  message("Got active times")
  return(data)
}

user.symptoms <- function(symptom_data, user) {
  team <- user_data$team_id[user_data$id == user]
  start_date <- cohortInfo(team)$startDate
  
  symptom_data <- filter(symptom_data, user_id == user) %>%
    select(user_id, occurred_at, knee_pain, stiffness) %>%
    mutate(occurred_at = as.Date(occurred_at, "%Y-%m-%d"),
           week = ceiling(as.numeric(occurred_at + 1 - start_date) / 7),
           pain_pc = 100 * knee_pain/knee_pain[1], 
           secondary_pc = 100 * stiffness/stiffness[1])
  
  symptom_data$week[symptom_data$week < 0] <- 0
  return(symptom_data)
}

user.weight <- function(weight_data, user) {
#   User_id <- as.numeric(as.character(user_data$id[paste(user_data$first_name,user_data$last_name) == user]))
  User_id <- user
  team <- user_data$team_id[user_data$id == User_id]
  start_date <- cohortInfo(team=team)$startDate
  
  data <- weight_data %>%
    filter(user_id == User_id) %>%
    mutate(created_at = as.Date(parse_iso_8601(created_at), "%Y-%m-%d"),
           week = ceiling(as.numeric(created_at + 1 - start_date) / 7)) %>%
    arrange(week)
  
  weight_goal <- (as.numeric(as.character(data$weight[1])) - 
                  as.numeric(as.character(user_data$weight_loss_goal[user_data$id == User_id]))) / 1000
  weight <- as.numeric(as.character(data$weight))/1000
  wt_goal <- rep(weight_goal, nrow(data))
  current_wk <- rep((ceiling(as.numeric(Sys.Date() + 1 - start_date) / 7)), nrow(data))
  
  weight_data <- data.frame("week" = data$week, "weight" = weight, "wt_goal" = wt_goal, "curr_wk" = current_wk)
  weight_data <- weight_data[order(weight_data$week), ]
  return(weight_data)

}

cohort.points <- function(population_data, team) {
  message("Fetching cohort points")
  
  cohort_size <- nrow(filter(user_data, team_id == team))
  start_date <- cohortInfo(team=team)$startDate

  data <- population_data %>%
    filter(user_id %in% user_data$id[user_data$team_id == team]) %>%
    select(occurred_at_local, points_awarded) %>%
    mutate(week = ceiling(as.numeric(as.Date(occurred_at_local) + 1 - start_date) / 7),
           points_awarded = as.numeric(as.character(points_awarded)),
           sum_points = cumsum(points_awarded)) %>%
    filter(week > 0) %>%
    arrange(occurred_at_local)
  weekly.data <- data.frame(matrix(ncol = 3, nrow = max(data$week)))
  names(weekly.data) <- c("week", "points", "mean_points")
  
  for(i in 1:max(data$week)){
    weekly.data[i,1] <- i
    weekly.data[i,2] <- sum(data$points_awarded[data$week == i])
  }
  weekly.data$mean_points <- weekly.data$points / cohort_size
  return(weekly.data)
}

cohort.activeSessions <- function(population_data, team) {
  message("Fetching active sessions")
  cohortSize <- nrow(filter(user_data, team_id == as.numeric(team)))

  start_date <- cohortInfo(team=team)$startDate
  
  data <- population_data %>%
    filter(user_id %in% user_data$id[user_data$team_id == team],
           points_awarded != 0) %>%
    select(occurred_at_local, points_awarded, user_id) %>%
    mutate(week = ceiling(as.numeric(as.Date(occurred_at_local) + 1 - start_date) / 7),
           occurred_at_local = as.Date(occurred_at_local)) %>%
    # filter(week > 0) %>%
    arrange(occurred_at_local) 
  
  # message(str(data))
  
  # Ensures only one workout per day is recorded
  data <- data[which(!duplicated(subset(data, select=-c(points_awarded)))), ]

  weekly.sessions <- data.frame(matrix(ncol = 2, nrow = max(data$week)))
  names(weekly.sessions) <- c("week", "active_sessions")

  for(i in 1:max(data$week)){
    weekly.sessions[i,1] <- i
    weekly.sessions[i,2] <- nrow(data[which(data$week == i), ]) / cohortSize
  }
  message("Active sessions fetched")
  return(weekly.sessions)
}

engagement <- function(population_data, team='p') {    
  message("Fetching engagement data")
  # check if population data is requested
  if (team=='p') {
    users <- filter(user_data, team_id %in% teams$id[teams$product == pathway]) %>% .$id
    message(paste(length(users),"people on the",pathway,"pathway"))
    data <- population_data[population_data$user_id %in% users, ] 
  } else {  # otherwise it's a team ID (cohort)
    message(paste("team = ",team))
    # get the user IDs of people in this team
    users <- filter(user_data, team_id == team) %>% .$id
    message(paste("cohort size: ",length(users)))
    # select the entries in population_data which are from users in this team
    data <- population_data[which(population_data$user_id %in% users), ] 
  }

  # only retain a few columns that are needed for measuring engagement. 
  # also retrieve created_at_local to make sure only data from week 0 onwards
  # is used
  data <- select(data, occurred_at_local, points_awarded, user_id) %>%
    filter(points_awarded != 0) %>%
    mutate(team = if (team=='p') {  # if population is requested, set team to team_id
             sapply(as.character(user_id), function(x) as.character(user_data$team_id[as.character(user_data$id) == x]))
           } else {
             team
           }) %>%
    filter(team %in% active_teams)  # only retain users in active teams (ones that are legitimate and with start date in the past)
  # store the start date for each user
  start_date <- if(team=='p'){sapply(as.character(data$team), function(x) cohortInfo(team=x)$startDate)}
              else{cohortInfo(team=team)$startDate}
  # store what week the exercise was performed in, subset the columns, only include exercise in week > 0,
  # and group by user_id and week. 
  data <- mutate(data, week = ceiling(as.numeric(as.Date(occurred_at_local) + 1 - start_date) / 7)) %>%
    select(occurred_at_local, user_id, week, team) %>%
    filter(week > 0) %>%
    arrange(week, team) %>%
    group_by(user_id, week)  
  
  # Filter out sessions that happened on the same day.
  # summarise the data by counting the number of sessions for each user in each week 
  data <- data %>%
    mutate(weekday = weekdays(as.Date(occurred_at_local))) %>%  # add a row with weekday the exercise happened
    select(-occurred_at_local) %>%
    dplyr::distinct(weekday) %>%  # you only have to put in weekday here because it's looking for distinct rows within the user_id/week groups
    dplyr::summarise(sessions = n()) %>%
    ungroup()
  
  
  #----------------------------------------------------------------------------------------------------------------------
  #------------------------------------ENGAGEMENT-------------------------------------------------------------
  
  # store for each (user, week) what current week they are in
  if (team == 'p') {
    data$curr_wk <- as.numeric(sapply(data$user_id, function(x) ceiling(as.numeric(Sys.Date() + 1 - cohortInfo(team=user_data$team_id[as.character(user_data$id) == x])$startDate) / 7 )))
  } else {
    data$curr_wk <- as.numeric(sapply(data$user_id, function(x) ceiling(as.numeric(Sys.Date() + 1 - cohortInfo(team=team)$startDate) / 7 )))
  }
  # get vector for user farthest in so all data will fit on the plot
  weeks <- 1:max(as.numeric(data$week))
  
  # for each user in the population, get their current week
  curr_wk <- sapply(users, function(y) ceiling(as.numeric(Sys.Date() + 1 - cohortInfo(team=user_data$team_id[as.character(user_data$id) == y])$startDate) / 7 ))
  # returns a vector as long as weeks. For each week, counts the number of users that have gone through that week, I think?
  population <- sapply(weeks, function(y) length(curr_wk[curr_wk > y]))
  
  # 
  active.n.m <- function(x, n, m, every.week=FALSE) {
    # x = data frame with number of days active for each (user, week) 
    pop_pc <- rep(0,max(weeks))
    if (every.week == TRUE) {
      d <- x[x$sessions >= n,]
      for (i in 1:max(weeks)) {
        if (i >= m) {
          a = sum(sapply(as.character(users), function(y) 1 * (nrow(d[as.character(d$user_id) == y & d$week %in% seq(i-m+1,i),]) == m)))
        } else {
          a = 0
        }
        pop_pc[i]<-as.integer(100 * a / population[i])
      }
    }
    else{
      for(i in 1:max(weeks)){
        d <- x[x$week %in% seq(max(1, i-m+1),i), ]
        d <- d[d$curr_wk > i, ] #avoids counting sessions but not people and thrfr overstating 
        a <- sum(sapply(as.character(users), function(y) 1 * (sum(d$sessions[as.character(d$user_id) == y]) >= n)))
        pop_pc[i]<- as.integer(100 * a / population[i])
        #message(pop_pc[i])
      }
    }
    df <- data.frame(cbind("Week" = weeks, "Percentage" = pop_pc))
    return(df)
  }
  
  #AT LEAST 1 ACTIVE DAY IN THE PAST WEEK
  engaged1.week <- active.n.m(data, 1, 1)
  
  #AT LEAST 2 ACTIVE DAYS IN THE PAST WEEK
  engaged2.week <- active.n.m(data, 2, 1)
  
  #DID 1 OR MORE ACTIVITIES IN THE PAST TWO WEEKS
  engaged.2week <- active.n.m(data, 1, 2)
  
  #AT LEAST 1 ACTIVE DAY/WK FOR PAST 2 WEEKS
  engaged2.2week <- active.n.m(data, 1, 2, every.week=TRUE)
  
  #4 OR MORE ACTIVE DAYS IN PAST 4 WEEKS
  engaged4.4week <- active.n.m(data, 4, 4)
  
  #mean.weekly.sessions <- sapply(unique(data$week), function(x) sum(data$sessions[data$week == x]) / population[weeks == x])
  mean.weekly.sessions <- sapply(unique(data$week), function(x) mean(data$sessions[data$week == x]))
  median.weekly.sessions <- sapply(unique(data$week), function(x) median(data$sessions[data$week == x]))
  
  engage.stats <- data.frame(cbind("Week" = engaged1.week$Week, 
                                   "pc_1x_last_week" = engaged1.week$Percentage, 
                                   "pc_2x_last_week" = engaged2.week$Percentage, 
                                   "pc_1x_last_2_weeks" = engaged.2week$Percentage, 
                                   "pc_2xweek_last_2_weeks" = engaged2.2week$Percentage, 
                                   "pc_4x_last_month" = engaged4.4week$Percentage, 
                                   "mean_weekly_physio" = mean.weekly.sessions, 
                                   "median_weekly_physio" = median.weekly.sessions))  
  
  return(engage.stats)
  
}

comment.data <- function(team='p') {
  message("Fetching feed engagement data")
  comments <- get.Data("comment")
  #data <- comments
  
  if(team == 'p'){
    active_users <- as.numeric(as.character(user_data$id[user_data$team_id %in% teams$id[teams$legitimate == TRUE & teams$product==pathway] ]))
  }
  else{
    active_users <- as.numeric(as.character(user_data$id[user_data$team_id == team]))
  }
  active_users <- active_users[!(active_users %in% c(non_starters,fake_users))]
  data <- comments[comments$author_id %in% active_users, ] 
  
  data$team <- sapply(as.character(data$author_id), function(x) as.character(user_data$team[as.character(user_data$id) == x]))
  start_date <- sapply(as.numeric(data$team), function(x) cohortInfo(team=x)$startDate)
  
  data <- select(data, feed_event_id,created_at,author_id,team) %>%
    mutate(created_at = as.Date(created_at, "%Y-%m-%d"),
           week = ceiling(as.numeric(created_at + 1 - start_date) / 7)) %>%
    filter(week > 0) %>%
    arrange(created_at)
  
  weeks <- 1:max(data$week)
  sum_comments <- sapply(weeks, function(x) nrow(data[data$week == x, ]))
  unique_data <- unique(subset(data, select=c(author_id, week)))
  no_commenters <- sapply(weeks, function(x) nrow(unique_data[unique_data$week == x, ]))
  ave_comments <- sum_comments / length(active_users)
  pc_commenters <- as.integer(100 * no_commenters / length(active_users))
  
  comments <- data.frame(cbind(weeks, sum_comments, ave_comments, no_commenters, pc_commenters))
  message(str(comments))
  return(comments)
}

wellness.data <- function(team, is.partial=F) {
  message("Crunching wellness data")
  message(paste("team =",team))
  # wellness_data <<- check.model("wellness_data", "health_report")
  
  if(team=='p'){
    users <- user_data$id[user_data$team_id %in% active_teams & user_data$team_id %in% teams$id[teams$product == pathway]]
    data <- health_report[health_report$user_id %in% users, ] 
  }
  else{
    users <- filter(user_data, team_id == team) %>% .$id
    data <- health_report[health_report$user_id %in% users, ]
  }
  
  data <- select(data, user_id, created_at, knee_pain, angle_used, stiffness, score) %>%
    mutate(created_at = as.Date(created_at, "%Y-%m-%d"),
           team = if(team=='p'){unname(sapply(as.character(user_id), function(x) as.character(user_data$team_id[as.character(user_data$id) == x])))}
                  else{team})
#            cohort = if(cohort=='p'){unlist(sapply(team, function(x) cohortInfo(team=x)$cohort), recursive=TRUE, use.names=FALSE)}
#                   else{cohort})
  
  start_date <- sapply(as.numeric(data$team), function(x) cohortInfo(team=x)$startDate)
  
  data <- mutate(data, week = ceiling(as.numeric(created_at + 1 - start_date) / 7)) %>%
    arrange(week, team)
  
  # all the wellness reports collected prior to week 0 are added to the wellness at week 0 (baseline)
  data$week[data$week < 0] <- 0
  message(paste("max week =",max(data$week)))
    
  weeks <- 0:max(data$week)
  if(team=='p'){
    current_week <- sapply(as.character(users), function(x) ceiling(as.numeric(Sys.Date() + 1 - cohortInfo(team=as.character(user_data$team_id[as.character(user_data$id) == x]))$startDate) / 7 ))
    population <- sapply(weeks, function(x) sum(current_week >= x))
  }
  else{
    current_week <- rep(ceiling(as.numeric(Sys.Date() + 1 - cohortInfo(team=team)$startDate) / 7 ))
    population <- length(users)
    message(paste("population = ",population))
  }

  if(is.partial==T){return(data)}
  
  user_id <- rep(users, max(data$week)+1)
  weeks <- rep(0:max(data$week), each=length(users))
  weekly.wellness <- data.frame(matrix(ncol=4, nrow=length(user_id)))
  names(weekly.wellness) <- if(pathway=="knee"){c("User_id", "Week", "Knee_pain", "Stiffness")}
                            else if(pathway=="back"){c("User_id", "Week", "Back_pain", "Impact")}
  weekly.wellness$User_id <- user_id
  weekly.wellness$Week <- weeks
  
  myfn <- function(x,y,z){mean(data[data$user_id == x & data$week == y , z], na.rm=TRUE)}  
  
  weekly.wellness <- filter(weekly.wellness, !is.na(User_id)) %>%
    mutate( Primary_outcome = mapply(myfn, x=User_id, y=Week, z="knee_pain"),
            Secondary_outcome = mapply(myfn, x=User_id, y=Week, z="stiffness")) 
  
  wellness_by_week <- dplyr::group_by(weekly.wellness, Week) %>%
    dplyr:: summarise(count = sum(!is.na(Primary_outcome)),
                      meanPrimaryOutcome = mean(Primary_outcome, na.rm=TRUE),
                      meanSecondaryOutcome = mean(Secondary_outcome, na.rm=TRUE)) %>%
    dplyr::mutate(pain_pc = as.integer(100 * meanPrimaryOutcome / meanPrimaryOutcome[1]),
                  secondaryOutcome_pc = as.integer(100 * meanSecondaryOutcome / meanSecondaryOutcome[1]),
                  population = population,
                  completion_rate = as.integer(100 * count / population))
  
  message(str(wellness_by_week))
  
  return(wellness_by_week)
}

education.data <- function(by, ID) {
  educationData <<- check.model("educationData", "article_read")
    
  data <- select(educationData, -device_type) %>%
    filter(complete.cases(.))
  message(str(data))
  
  
  if(by == 'user'){
    User_id <- as.numeric(as.character(user_data$id[paste(user_data$first_name,user_data$last_name) == ID]))
    data <- filter(data, user_id == User_id)
  } else if(by == 'cohort'){  
    data <- filter(data, user_id %in% user_data$id[user_data$team_id == ID]) %>%
      distinct(user_id, article_id)
    cohortSize <- nrow(filter(user_data, team_id == ID))
  } else {
    message("error in active.times, 'by' not selected")
  }
  
  message(str(data))
  
  article_completion <- data.frame("article_id"=matrix(c(1:max(data$article_id)))) %>%
    mutate(no_read = sapply(article_id, function(x) nrow(data[data$article_id == x,])),
              pc_read = as.integer( 100 * no_read / cohortSize))
  
  message(str(article_completion))
  
  return(article_completion) 
}

weight.data <- function (data=weightData, team='p') {
  if(team == 'p'){
    active_users <- as.numeric(as.character(user_data$id[user_data$team_id %in% teams$id[teams$legitimate == TRUE & teams$product=="knee"] ]))
  }
  else{
    active_users <- as.numeric(as.character(user_data$id[user_data$team_id == team]))
  }
  message(paste("population in cohorts =",length(active_users)))
  
  active_users <- active_users[!(active_users %in% c(non_starters,fake_users))]
  message(paste("real population =",length(active_users)))
  
  
  data <- data %>%
    mutate(team = as.numeric(sapply(as.character(user_id), function(x) as.character(user_data$team_id[as.character(user_data$id)==x])))) %>%
    filter(user_id %in% active_users) %>%
    mutate(startDate = as.Date(sapply(team, function(x) format(cohortInfo(team=x)$startDate, "%Y-%m-%d")), "%Y-%m-%d"),
           created_at = as.Date(created_at, "%Y-%m-%d"),   
           week = pmax(ceiling(as.numeric(created_at + 1 - startDate) / 7), 0),
           weight = as.numeric(as.character(weight))) %>%
    group_by(user_id, week) %>%
    summarise(wk_weight = mean(weight, na.rm=TRUE))
  
  
  data$week[data$week < 0] <- 0
  weeks <- 0:max(data$week)
  
  weightData <- data.frame(matrix(ncol=4, nrow=length(weeks)))
  names(weightData) <- c("week","pc_completion","wk_weight","pc_weight")
  
  weightData <- weightData %>%
    mutate(week = weeks,
           pc_completion = sapply(week, function(x) round(100 * nrow(data[data$week==x,]) / length(active_users))))    
  
  
  data <- spread(data, user_id, wk_weight)    
  data <- na.locf(data)
  
  data <- melt(data, id.vars="week", value.name="weight") %>%
    group_by(week) %>%
    summarise(mean_weight = mean(weight, na.rm=TRUE))
  
  weightData <- weightData %>%
    mutate(wk_weight = data$mean_weight,
           pc_weight = 100 * wk_weight / wk_weight[1])
  
  return(weightData)
}


kneeArticlesMatrix <- as.matrix(cbind("ID"=1:22, "Title"=c("What is osteoarthritis?", 
                                                           "Treating osteoarthritis: The basics.", 
                                                           "Physiotherapy - why it helps.", 
                                                           "Aerobic exercise for osteoarthritis.", 
                                                           "Stretching and strengthening: the quadriceps.", 
                                                           "Weight loss for osteoarthritis.", 
                                                           "Setting goals to conquer knee pain.", 
                                                           "Eating well for weight loss.", 
                                                           "Ones to miss: Seven foods and drinks to avoid when losing weight.", 
                                                           "Osteoarthritis myths volume 1: The five big ones.", 
                                                           "Osteoarthritis myths volume 2: Food, drink & drugs.", 
                                                           "Getting your head in the game: Mental barriers to weight loss.", 
                                                           "No more excuses: Beating practical barriers to weight loss.", 
                                                           "Famous figures throughout history with osteoarthritis.", 
                                                           "Tackling mental health in osteoarthritis.", 
                                                           "Osteoarthritis, anxiety and depression: An introduction.", 
                                                           "End-stage osteoarthritis and knee replacement: The facts.", 
                                                           "Pharmacologic Treatment of Osteoarthritis.", 
                                                           "Osteoarthritis in the animal kingdom.", 
                                                           "Breaking bad food habits.", 
                                                           "Osteoarthritis in the workplace.", 
                                                           "Osteoarthritis and you: A look back at how far you've come."
                                                           )))
backArticlesMatrix <- as.matrix(cbind("ID"=1:15, "Title"=c("Welcome to the Hinge Health digital care programme for back pain",
                                                           "Lower back pain: The basics",
                                                           "Getting active: the best first step for beating back pain",
                                                           "Pacing yourself for success: Avoiding over- and under-activity",
                                                           "Getting moving with back pain: at work and beyond",
                                                           "The psychology of lower back pain",
                                                           "Back pain, communication and relationships",
                                                           "Back pain: myths and misconceptions",
                                                           "Pharmacologic treatments for lower back pain",
                                                           "Lower back pain and surgery: the facts",
                                                           "Overcoming practical barriers to tackling back pain",
                                                           "Practical tactics to avoid back pain flareups",
                                                           "10 tips to maintaining your progress",
                                                           "Learning pain acceptance",
                                                           "Looking back on all you've learned"
                                                           )))


# population.medals <- function() {
#   cohort_info <- data.frame(c(1,2,3),c(36,NA,38))
#   names(cohort_info) <- c("cohort","start_week")
#   cohort_info$members <- sapply(cohort_info$cohort, function(x) nrow(user_data[user_data$team_id == x, ]))
#   cohort_info$current_week <- as.numeric(format(Sys.Date(), "%U")) - cohort_info$start_week + 1
#   
#   data <- get.Data("award") 
#   data <- mutate(data, team = lapply(as.numeric(as.character(data$user_id)), function(x) user_data$team_id[user_data$id == x]))
#   
#   
#   #data$team <- as.numeric(as.character(lapply(as.numeric(as.character(data$user_id)), function(x) user_data$team_id[which(as.numeric(as.character(user_data$id)) == x)])))
#   data$start_week <- as.numeric(as.character(lapply(data$team, function(x) cohort_info$start_week[which(cohort_info$cohort == x)])))
#   data$week <- as.numeric(format(as.Date(data$created_at)-1, "%U"))
#   
#   data$award_week <- data$week - data$start_week + 1
#   data$first_medal <- sapply(data$user_id, function(x) min(as.numeric(as.character(data$award_week[which(data$user_id == x)]))))
#   first_medal <- unique(subset(data, select=c(user_id, first_medal)))
#   data <- subset(data, select=-c(id,created_at,updated_at,start_week,week)) 
#   
#   award.data <- data.frame(matrix(nrow=max(data$award_week), ncol=7))
#   names(award.data) <- c("week", "population", "education", "points_gained", "week_goals", "posts", "any")
#   
#   award.data$week <- seq(1:max(data$award_week))
#   for(i in 1:nrow(award.data)){
#     award.data$population[i] <- sum(cohort_info$members[which(cohort_info$current_week >= i)])
#     award.data$education[i] <- length(data$award_definition_id[which(data$award_definition_id == "4" & data$award_week == i)]) 
#     award.data$points_gained[i] <- length(data$award_definition_id[which(data$award_definition_id == "1" & data$award_week == i)])
#     award.data$week_goals[i] <- length(data$award_definition_id[which(data$award_definition_id == "2" & data$award_week == i)])
#     award.data$posts[i] <- length(data$award_definition_id[which(data$award_definition_id == "3" & data$award_week == i)])
#     award.data$any[i] <- nrow(first_medal[which(first_medal$first_medal == i), ])
#   }
#   
#   award.data[, 3:7] <- cumsum(award.data[, 3:7])
#   award.data[, 3:7] <- 100 * award.data[, 3:7] / max(award.data$population)
#   return(award.data) 
# }
